from audio2numpy import open_audio

#file 1 
fp1 = "Classic_C_Major_Op_36_Spiritoso_68383705.mp3"  # change to the correct path to your file accordingly
signal1, sr1 = open_audio(fp1)
#file 2
fp2 = "Classic_Lyudvig_van_Betkhoven_in_G_Major_HWV_441_68383713.mp3"  # change to the correct path to your file accordingly
signal2, sr2 = open_audio(fp2)

import numpy as np
from scipy.io.wavfile import write

#lenght of the signal1
print (len(signal1))

#lenght of the signal2
print (len(signal2))

signal1 = signal1[:3000000]
signal2 = signal2[:3000000]
signal=signal1+signal2

#first melody
write('t1.wav', sr1, signal1)
#second melody
write('t2.wav', sr2, signal2)
#mixed
write('t3.wav', sr1, signal)

#вывод аудиофайла
ipd.Audio('t3.wav')