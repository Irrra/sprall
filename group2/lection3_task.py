spisoc1 = {0: 'ноль', 1: 'один', 2: 'два', 3: 'три', 4: 'четыре', 5: 'пять', 6: 'шесть', 7: "семь", 8: "восемь",
           9: 'девять'}
spisoc2 = {10: 'десять', 11: 'одиннадцать', 12: 'двенадцать', 13: 'тринадцать', 14: 'четырнадцать', 15: 'пятнадцать',
           16: 'шестнадцать', 17: 'семнадцать', 18: 'восемнадцать', 19: 'девятнадцать', 20: 'двадцать', 30: 'тридцать',
           40: 'сорок', 50: 'пятьдесят', 60: 'шестьдесят', 70: 'семьдесят',
           80: 'восемьдесят', 90: 'девяносто'}
spisoc3 = {100: 'сто', 200: 'двести', 300: 'триста', 400: 'четыреста', 500: 'пятьсот', 600: 'шестьсот',
           700: 'семьсот', 800: 'восемьсот', 900: 'девятьсот'}

numbers = {"один": 1, "два": 2, "три": 3, "четыре": 4, "пять": 5, "шесть": 6, "семь": 7, "восемь": 8, "девять": 9}
numten = {"десять": 1, "одиннадцать": 1, "двенадцать": 1, "тринадцать": 1, "четырнадцать": 1,
          "пятнадцать": 1, "шестнадцать": 1, "семнадцать": 1, "восемнадцать": 1, "девятнадцать": 1}
numtens = {"одиннадцать": 1, "двенадцать": 2, "тринадцать": 3, "четырнадцать": 4,
           "пятнадцать": 5, "шестнадцать": 6, "семнадцать": 7, "восемнадцать": 8, "девятнадцать": 9}
numdesyatki = {"двадцать": 2, "тридцать": 3, "сорок": 4, "пятьдесят": 5, "шестьдесят": 6, "семьдесят": 7,
               "восемьдесят": 8, "девяносто": 9}
numsotni = {"сто": 1, "двести": 2, "триста": 3, "четыреста": 4, "пятьсот": 5, "шестьсот": 6, "семьсот": 7,
            "восемьсот": 8, "девятьсот": 9}
millions = ["миллион", "миллиона", "миллионов"]
thousands = ["тысяча", "тысяч", "тысячей"]

def num2txt(a):
    yk = ''

    if a // 100 > 0:
        yk += spisoc3[a - a % 100] + " "
    if a % 100 // 10 > 0:
        if a % 100 // 10 == 1:
            yk += spisoc2[a % 100]
        else:
            yk += spisoc2[a % 100 - a % 10] + " "
    if a % 100 // 10 != 1 and a % 10 > 0:
        yk += spisoc1[a % 10]

    return yk

def txt2lib(self):
    spisok = self.split()
    newsp = []
    for elem in spisok:
        if elem in numbers:
            newsp.append({"num": numbers.get(elem), "level": 1})
        if elem in numten:
            newsp.append({"num": numten.get(elem), "level": 2})
            newsp.append({"num": numtens.get(elem), "level": 1})
        if elem in numdesyatki:
            newsp.append({"num": numdesyatki.get(elem), "level": 2})
        if elem in numsotni:
            newsp.append({"num": numsotni.get(elem), "level": 3})
        if elem in millions:
            newsp.append({"num": 1, "level": 7})
        if elem in thousands:
            newsp.append({"num": 1, "level": 4})

    return newsp


def lib2num(train):
    normalNums = []
    lastNum = 10
    lastBigNum = 10
    s = []

    for i in train:
        f = False
        if i["level"] in (1, 2, 3):
            if lastNum > i["level"]:
                lastNum = i["level"]
            else:
                f = True
        else:
            if lastBigNum > i["level"]:
                lastBigNum = i["level"]
                lastNum = 10
            else:
                f = True
        if f:
            lastBigNum = 10
            lastNum = 10
            normalNums.append(s)
            s = []
        s.append(i)
    if s != []:
        normalNums.append(s)
    a = []

    for num in normalNums:
        k = 0
        s = 0

        for n in num:
            if n["level"] in (1, 2, 3):
                h = 10 ** (n["level"] - 1)
                k += n["num"] * h
            else:
                h = 10 ** (n["level"] - 1)
                if k == 0:
                    s += h
                else:
                    s += k * h
                    k = 0
        s += k
        a.append(s)

    return "".join(str(i) for i in a)

def txt2num(txt):
    return lib2num(txt2lib(txt))


if __name__ == "__main__":
    import pandas as pd

    inp = pd.read_csv("lection3_train.csv")

    mistakes = 0

    for n in inp["number"]:
        num = int(str(n)[:3])
        try:
            a = num2txt(num)
            b = txt2num(a)
            if str(num) != b:
                print(str(num), b)
                mistakes += 1
        except KeyError:
            mistakes += 1
            print(f"error {num}")

    print(f"Ошибки: {mistakes} из {len(inp['number'])}")