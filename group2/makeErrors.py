from random import randint, shuffle


def createErrVer(word, countOfErr = 1):
    # немного парных согласных и распростронненых замен
    lets = [
        ("е", "э"),
        ("г", "в"),
        ("б", "п"),
        ("г", "к"),
        ("д", "т"),
        ("з", "с"),
        ("в", "ф"),
        ("ж", "ш"),
        ("а", "о"),
        ("и", "е")
    ]
    letsHave = tuple([i[0] for i in lets] + [i[1] for i in lets])

    answer = ""

    for _ in range(countOfErr):
        i = randint(0, len(word) - 1)
        while word[i] not in letsHave:
            i = randint(0, len(word) - 1)

        for a, b in lets:
            if word[i] == a:
                answer = word[:i] + b + word[i + 1:]
                break
            elif word[i] == b:
                answer = word[:i] + a + word[i + 1:]
                break

    return answer

for w in ["один", "тысяча", "миллион", "восемь"]:
    print(f"]w")
    for i in range(10):
        print(createErrVer(w))