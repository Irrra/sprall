import os
from random import randint
from PIL import Image, ImageDraw  # Подключим необходимые библиотеки.


def make_img(img_name):
    # Открываем изображение.
    image = Image.open(f'group1/task_lection_10/data/{img_name}')
    draw = ImageDraw.Draw(image)  # Создаем инструмент для рисования.
    width = image.size[0]  # Определяем ширину.
    height = image.size[1]  # Определяем высоту.
    pix = image.load()  # Выгружаем значения пикселей.

    factor = randint(50, 100)
    for i in range(width):
        for j in range(height):
            rand = randint(-factor, factor)
            a = pix[i, j][0] + rand
            b = pix[i, j][1] + rand
            c = pix[i, j][2] + rand
            if (a < 0):
                a = 0
            if (b < 0):
                b = 0
            if (c < 0):
                c = 0
            if (a > 255):
                a = 255
            if (b > 255):
                b = 255
            if (c > 255):
                c = 255
            draw.point((i, j), (a, b, c))
    if not os.path.exists('group1/task_lection_10/build'):
        os.mkdir('group1/task_lection_10/build')
    image.save(f'group1/task_lection_10/build/{img_name}')


if __name__ == "__main__":
    images = os.listdir('group1/task_lection_10/data')
    for img_name in images:
        make_img(img_name)
