from random import randint, choices, choice


def make_a_mistake_in_word(s, number_mistakes=1):
    """Возвращает слово с ошибкой
       Например: один -> адин    
    """
    functions = [lambda x: remove_letter(x), lambda x: replace_letters(
        x, replaced='о', replaced_by='а'), lambda x: replace_letters(
        x, replaced='е', replaced_by='и'), lambda x:  replace_letters(
        x, replaced='и', replaced_by='е'), lambda x:  replace_letters(
        x, replaced='н', replaced_by='нн'), lambda x:  replace_letters(
        x, replaced='дцать', replaced_by='цать'), lambda x:  replace_letters(
        x, replaced='ь', replaced_by=''), lambda x:  replace_letters(
        x, replaced='нн', replaced_by='н'), ]
    selected_functions = choices(functions, k=number_mistakes)
    for func in selected_functions:
        if func(s) == -1:
            selected_functions.append(choice(functions))
        else:
            s = func(s)
    return s


def remove_letter(s):
    a = list(s)
    del a[randint(0, len(s) - 1)]
    return ''.join(a)


def replace_letters(s, replaced='а', replaced_by='о'):
    if replaced not in s:
        return -1
    else:
        index = [i for i in range(len(s)) if s[i] == replaced]
        i = choice(index)
        if i + 1 < len(s):
            return s[:i] + replaced_by + s[i + 1:]
        else:
            return s[:i] + replaced_by

def num2text(num, len_num=3):
    ''' Число типа int превращает в строку с числительным'''
    dict = {0: 'ноль', 1: 'один', 2: 'два', 3: 'три', 4: 'четыре', 5: 'пять', 6: 'шесть',
            7: 'семь', 8: 'восемь', 9: 'девять', 10: 'десять', 11: 'одиннадцать', 12: 'двенядцать',
            13: 'тринадцать', 14: 'четырнадцать', 15: 'пятнадцать', 16: 'шестнадцать',
            17: 'семнадцать', 18: 'восемнадцать', 19: 'девятнадцать', 20: 'двадцать',
            30: 'тридцать', 40: 'сорок', 50: 'пятьдесят', 60: 'шестьдесят', 70: 'семьдесят', 80: 'восемьдесят',
            90: 'девяноста', 100: 'сто', 200: 'двести', 300: 'триста', 400: 'четыреста', 500: 'пятьсот',
            600: 'шестьсот', 700: 'семьсот', 800: 'восемьсот', 900: 'девятьсот'}
    res = ['ноль'] * (len_num - len(str(num)))
    str_num = str(num)
    while str_num != '':
        if int(str_num) in dict.keys():
            res += [dict[int(str_num)]]
            str_num = ''
        else:
            res += [dict[int(str_num[0]) * (10**(len(str_num) - 1))]]
            str_num = str_num[1:]
    return ' '.join(res)

def damerau_levenshtein_distance(s1, s2):
    d = {}
    lenstr1 = len(s1)
    lenstr2 = len(s2)
    for i in range(-1,lenstr1+1):
        d[(i,-1)] = i+1
    for j in range(-1,lenstr2+1):
        d[(-1,j)] = j+1
 
    for i in range(lenstr1):
        for j in range(lenstr2):
            if s1[i] == s2[j]:
                cost = 0
            else:
                cost = 1
            d[(i,j)] = min(
                           d[(i-1,j)] + 1, # deletion
                           d[(i,j-1)] + 1, # insertion
                           d[(i-1,j-1)] + cost, # substitution
                          )
            if i and j and s1[i]==s2[j-1] and s1[i-1] == s2[j]:
                d[(i,j)] = min (d[(i,j)], d[i-2,j-2] + cost) # transposition
 
    return d[lenstr1-1,lenstr2-1]