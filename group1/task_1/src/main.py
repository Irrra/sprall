import os
from tests.test_system import create_tests, test_system


def text2num(s):
    """Преобразовывает числительное строкового представления в число с типом int
        Например: семьдесят шесть -> 76
    """
    data_numbers = {'ноль': (0, 0), 'один': (1, 0), 'два': (2, 0), 'три': (3, 0), 'четыре': (4, 0), 'пять': (5, 0),
                    'шесть': (6, 0), 'семь': (7, 0), 'восемь': (8, 0), 'девять': (9, 0), 'десять': (10, 1),
                    'одиннадцать': (11, 1),
                    'двенядцать': (12, 1), 'тринадцать': (13, 1), 'четырнадцать': (14, 1), 'пятнадцать': (15, 1),
                    'шестнадцать': (16, 1), 'семнадцать': (17, 1), 'восемнадцать': (18, 1), 'девятнадцать': (19, 1),
                    'двадцать': (20, 1), 'тридцать': (30, 1), 'сорок': (40, 1), 'пятьдесят': (50, 1),
                    'шестьдесят': (60, 1),
                    'семьдесят': (70, 1), 'восемьдесят': (80, 1), 'девяноста': (90, 1), 'сто': (100, 2),
                    'двести': (200, 2),
                    'триста': (300, 2), 'четыреста': (400, 2), 'пятьсот': (500, 2), 'шестьсот': (600, 2),
                    'семьсот': (700, 2),
                    'восемьсот': (800, 2), 'девятьсот': (900, 2)}

    res = []
    last_word = ''
    data = s.lower().split()
    len_data = len(data)
    for i in range(len_data):
        word = data[i]
        if last_word:
            if word == 'ноль':
                res.append(data_numbers[word][0])
                last_word = data[i]
            else:
                if data_numbers[word][1] < data_numbers[last_word][1]:
                    res[-1] += data_numbers[word][0]
                    last_word = data[i]
                else:
                    res.append(data_numbers[word][0])
                    last_word = data[i]

        else:
            res.append(data_numbers[word][0])
            last_word = data[i]
    res = ''.join([str(i) for i in res])
    return res


if __name__ == "__main__":
    tests_file_name = 'tests.txt'
    output_file_name = 'output.txt'
    verdict_file_name = 'verdict.txt'
    # создаем файл с тестами
    create_tests(count=15000)
    tests = open(f'group1/task_1/src/tests/{tests_file_name}', 'r', encoding='utf-8')
    if not os.path.exists('group1/task_1/build'):
        os.mkdir('group1/task_1/build')
    output = open(f'group1/task_1/build/{output_file_name}', 'w')
    for i in tests:
        s = i.rstrip('\n').split('_')[1]
        # записываем в файл результат функции text2num
        output.write(
            f"{text2num(s.split('/')[0])} {text2num(s.split('/')[1])} {text2num(s.split('/')[2])}" + "\n")
    # запускаем тестирующую систему
    test_system(output_file_name=f'group1/task_1/build/{output_file_name}',
                tests_file_name=f'group1/task_1/src/tests/{tests_file_name}', verdict_file_name=f'group1/task_1/src/tests/{verdict_file_name}')
    tests.close()
    output.close()
